package com.avalogics.myshot;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.avalogics.myshot.Adapters.CustomAdapter;
import com.avalogics.myshot.Fragments.BlankFragment;
import com.avalogics.myshot.Fragments.BlankFragment2;
import com.avalogics.myshot.Log.LoginActivity;
import com.avalogics.myshot.Log.Mod_Photo;
import com.avalogics.myshot.Networking.GetDataService;
import com.avalogics.myshot.Networking.RetrofitClientInstance;
import com.avalogics.myshot.model.Pojos.OffersD;
import com.avalogics.myshot.layouts.Commerc_details;
import com.avalogics.myshot.layouts.MyLocation;
import com.avalogics.myshot.layouts.Special_Offers;
import com.avalogics.myshot.prefs.SessionPrefs;
import com.facebook.FacebookSdk;

import java.io.InputStream;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private CustomAdapter adapter;
    private RecyclerView recyclerView;
    ProgressDialog progressDoalog;
    private List <OffersD> offers;
    private Button btnbar,btnrest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        FacebookSdk.sdkInitialize( this );
        setContentView( R.layout.activity_main );
        android.support.v7.widget.Toolbar toolbar = findViewById( R.id.toolbar );
        setSupportActionBar( toolbar );

        recyclerView = findViewById( R.id.recyclerView );
        btnbar = findViewById( R.id.btnbares );
        btnrest = findViewById( R.id.btnrest );

        /*********************************Login****************************************/
        if (!SessionPrefs.get( this ).isLoggedIn()) {
            startActivity( new Intent( this, LoginActivity.class ) );
            finish();
            return;
        }

        if (!SessionPrefs.get( this ).isLoggedIn()) {
            startActivity( new Intent( this, LoginActivity.class ) );
            finish();
            return;
        }

        if (SessionPrefs.get( this ).isLoggedIn()) {
            BlankFragment2 blanck = new BlankFragment2();
            getSupportFragmentManager().beginTransaction()
                    .replace( R.id.container_layout, blanck )
                    .addToBackStack( null ).commit();
        }

        DrawerLayout drawer = ( DrawerLayout ) findViewById( R.id.drawer_layout );
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle( this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close );
        drawer.addDrawerListener( toggle );
        toggle.syncState();

        NavigationView navigationView = ( NavigationView ) findViewById( R.id.nav_view );
        navigationView.setNavigationItemSelectedListener( this );


        btnbar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BlankFragment2 cameraImportFragment = new BlankFragment2();
                getSupportFragmentManager().beginTransaction()
                        .replace( R.id.container_layout, cameraImportFragment )
                        .addToBackStack( null )
                        .commit();
            }
        } );

        btnrest.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BlankFragment blanck = new BlankFragment();
                getSupportFragmentManager().beginTransaction()
                        .replace( R.id.container_layout, blanck )
                        .addToBackStack( null )
                        .commit();
            }
        } );

        /*****************************API DATA CALLING******************************************/
        progressDoalog = new ProgressDialog( MainActivity.this );
        progressDoalog.setMessage( "Loading...." );
        progressDoalog.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class );
        Call <List <OffersD>> call = service.getData();
        call.enqueue( new Callback <List <OffersD>>() {
            @Override
            public void onResponse(Call <List <OffersD>> call, Response <List <OffersD>> response) {
                progressDoalog.dismiss();
                generateDataList( response.body() );
            }

            @Override
            public void onFailure(Call <List <OffersD>> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText( MainActivity.this, "Parece que tenemos problemas, porfavor intente mas tarde", Toast.LENGTH_SHORT ).show();
            }
        } );

        /****************************************Facebook LO*************************************************/

    }

    /**********************Method  for api****************************/
    private void generateDataList(List <OffersD> body) {

        recyclerView = findViewById(R.id.recyclerView);
        adapter = new CustomAdapter(this,body);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);

    }
        /****** method for facebook Login***********************/


    public class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }



        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    public void onBackPressed() {
    super.onBackPressed();
        Intent i = new Intent( MainActivity.this,MainActivity.class );
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate( R.menu.main, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected( item );
    }

    /**********Lateral menu items************/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragment = null;

        if (id == R.id.nav_camera) {
            Intent i = new Intent( MainActivity.this, MyLocation.class );
            startActivity( i );

        } else if (id == R.id.nav_gallery) {
            Intent i = new Intent( MainActivity.this, Special_Offers.class );
            startActivity( i );

        } else if (id == R.id.nav_slideshow) {
            Intent intent = new Intent( MainActivity.this, Mod_Photo.class );
            startActivity( intent );


        } else if (id == R.id.nav_manage) {

            Intent intent = new Intent( MainActivity.this,LoginActivity.class );
            startActivity( intent );
            finish();

        } else if (id == R.id.nav_share) {


        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = ( DrawerLayout ) findViewById( R.id.drawer_layout );
        drawer.closeDrawer( GravityCompat.START );
        return true;
    }
}
