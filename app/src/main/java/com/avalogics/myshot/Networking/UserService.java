package com.avalogics.myshot.Networking;


import com.avalogics.myshot.model.Affiliate;
import com.avalogics.myshot.model.Comment_Body;
import com.avalogics.myshot.model.LoginBody;
import com.avalogics.myshot.model.RegisterBody;
import com.avalogics.myshot.model.modelpack.Comment_;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {


    String BASE_URL = "http://ec2-18-191-167-26.us-east-2.compute.amazonaws.com:5012";

    @POST("/api/users/login/")
    Call<Affiliate> login(@Body LoginBody loginBody);

    @POST("/api/users")
    Call<Affiliate> register(@Body RegisterBody registerBody);

    @POST("/api/{_id}/comments")
    Call<Comment_> comment();



}
