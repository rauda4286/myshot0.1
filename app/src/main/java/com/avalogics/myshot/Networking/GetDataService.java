package com.avalogics.myshot.Networking;

import com.avalogics.myshot.model.Pojos.Comments;
import com.avalogics.myshot.model.Pojos.Offer;
import com.avalogics.myshot.model.Pojos.OffersD;
import com.avalogics.myshot.model.modelpack.Comment_;
import com.avalogics.myshot.model.modelpack.Offert;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataService {

    @GET("/api/places")
    Call<List <OffersD>> getData();

    @GET("/api/offers")
    Call<List <Offer>> getDetails();

    @GET("/api/places/5b364ca3718d2253648fd468/comments")
    Call <Comments> getComment();

    @GET("/api/offers/list")
    Call <List<Offert>>getOffers();



}
