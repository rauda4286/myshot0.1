package com.avalogics.myshot.Log;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avalogics.myshot.MainActivity;
import com.avalogics.myshot.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

public class LogGm_Activity extends AppCompatActivity {


  /*  private static final String TAG = MainActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;

    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;

    private SignInButton btnSignIn;
    private LinearLayout llProfileLayout;
    private ImageView imgProfilePic;
    private TextView txtName, txtEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );

        btnSignIn = ( SignInButton ) findViewById( R.id.btn_sign_in );


        btnSignIn.setOnClickListener( this );

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder( GoogleSignInOptions.DEFAULT_SIGN_IN )
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder( this )
                .enableAutoManage( this, this )
                .addApi( Auth.GOOGLE_SIGN_IN_API, gso )
                .build();

        // Customizing G+ button
        btnSignIn.setSize( SignInButton.SIZE_STANDARD );
        btnSignIn.setScopes( gso.getScopeArray() );


    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent( mGoogleApiClient );
        startActivityForResult( signInIntent, RC_SIGN_IN );
    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d( TAG, "handleSignInResult:" + result.isSuccess() );
        if (result.isSuccess()) {

            GoogleSignInAccount acct = result.getSignInAccount();
            Log.e( TAG, "display name: " + acct.getDisplayName() );

            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();

            Log.e( TAG, "Name: " + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl );

            txtName.setText( personName );
            txtEmail.setText( email );
            Glide.with( getApplicationContext() ).load( personPhotoUrl )
                    .thumbnail( 0.5f )
                    .crossFade()
                    .diskCacheStrategy( DiskCacheStrategy.ALL )
                    .into( imgProfilePic );

            updateUI( true );
        } else {
            // Signed out, show unauthenticated UI.
            updateUI( false );
        }
    }

    @Override
    public void onClick(View v) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode, resultCode, data );

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent( data );
            handleSignInResult( result );
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            btnSignIn.setVisibility( View.GONE );
            llProfileLayout.setVisibility( View.VISIBLE );
        } else {
            btnSignIn.setVisibility( View.VISIBLE );
            llProfileLayout.setVisibility( View.GONE );
        }
    }*/
}