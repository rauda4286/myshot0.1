package com.avalogics.myshot.Log;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avalogics.myshot.MainActivity;
import com.avalogics.myshot.Networking.UserService;
import com.avalogics.myshot.R;
import com.avalogics.myshot.model.Affiliate;
import com.avalogics.myshot.model.ApiError;
import com.avalogics.myshot.model.RegisterBody;
import com.avalogics.myshot.prefs.SessionPrefs;
import com.avalogics.myshot.model.BodyRegister;
import com.facebook.FacebookSdk;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Register extends AppCompatActivity {

    private Retrofit RestAdapter;
    private UserService mUserService;

    // UI references.
    private ImageView mLogoView;
    private EditText UserName;
    private EditText PasswordView;
    private EditText Email;
    private String Facebook_id;

    private EditText PasswordView2;

    private Button SigUpnButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_register );

        UserName = findViewById( R.id.user_name_register);
        PasswordView = findViewById( R.id.password_register);
        PasswordView2 = findViewById( R.id.password2_register);
        Email = findViewById( R.id.email_register );

        SigUpnButton = findViewById( R.id.email_sign_up_button_register);


        // Crear adaptador Retrofit
        RestAdapter = new Retrofit.Builder()
                .baseUrl( UserService.BASE_URL)
                .addConverterFactory( GsonConverterFactory.create())
                .build();

        // Crear conexión a la API
        mUserService = RestAdapter.create(UserService.class);

        PasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    if (!isOnline()) {
                        showLoginError(getString( R.string.error_network));
                        return false;
                    }
                    attemptRegister();
                    return true;
                }
                return false;
            }
        });
        PasswordView2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    if (!isOnline()) {
                        showLoginError(getString( R.string.error_network));
                        return false;
                    }
                    attemptRegister();
                    return true;
                }
                return false;
            }
        });

        SigUpnButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isOnline()) {
                    showLoginError(getString( R.string.error_network));
                    return;
                }
                attemptRegister();

            }
        });
    }

    private void attemptRegister() {

        // Store values at the time of the login attempt.

        String username = UserName.getText().toString();
        String password = PasswordView.getText().toString();
        String password2 = PasswordView2.getText().toString();
        String email = Email.getText().toString();





        boolean cancel = false;

        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password) ) {
            PasswordView.setError(getString( R.string.error_field_required));
            focusView = PasswordView;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            PasswordView.setError(getString( R.string.error_invalid_password));
            focusView = PasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(password2)) {
            PasswordView2.setError(getString( R.string.error_field_required));
            focusView = PasswordView2;
            cancel = true;

        }else  if(password != password2){
            Toast.makeText( this, "Verifique que las contraseñas sean identicas", Toast.LENGTH_SHORT ).show();
            focusView = PasswordView;
            cancel=true;
        }


        // Verificar si el user tiene contenido.
        if (TextUtils.isEmpty(username)) {
            UserName.setError(getString( R.string.error_field_required));
            focusView = UserName;
            cancel = true;
        }


        if (TextUtils.isEmpty(email)) {
            Email.setError(getString( R.string.error_field_required));
            focusView = Email;
            cancel = true;
        }

        /**facebook_app_id="127265894795075"**/





        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            Call<Affiliate> registerCall = mUserService.register(new RegisterBody(username, password,email));
            registerCall.enqueue(new Callback<Affiliate>() {
                @Override
                public void onResponse(Call<Affiliate> call, Response<Affiliate> response) {

                    // Procesar errores
                    if (!response.isSuccessful()) {
                        String error = "Ha ocurrido un error. Contacte al administrador";
                        if (response.errorBody()
                                .contentType()
                                .subtype()
                                .equals("json")) {
                            ApiError apiError = ApiError.fromResponseBody(response.errorBody());

                            error = apiError.getMessage();
                            Toast.makeText( Register.this, "Porfavor verifique que la contraseña coincida", Toast.LENGTH_SHORT ).show();
                            //Log.d("RegisterActivity", apiError.getDeveloperMessage());
                        } else {
                            try {
                                // Reportar causas de error no relacionado con la API
                                Log.d("RegisterActivity", response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        showLoginError(error);
                        return;
                    }

                    // Guardar registrado en preferencias
                    SessionPrefs.get( Register.this).saveAffiliate(response.body());

                    showAppointmentsScreen();
                }

                @Override
                public void onFailure(Call<Affiliate> call, Throwable t) {
                    showLoginError(t.getMessage());
                }
            });
        }
    }



    private boolean isPasswordValid(String password) {
        return password.length() > 3;
    }



    private void showAppointmentsScreen() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void showLoginError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager ) getSystemService( Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }




}
