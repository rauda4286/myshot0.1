package com.avalogics.myshot.Log;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avalogics.myshot.MainActivity;
import com.avalogics.myshot.Networking.UserService;
import com.avalogics.myshot.R;
import com.avalogics.myshot.model.Affiliate;
import com.avalogics.myshot.model.ApiError;
import com.avalogics.myshot.model.LoginBody;
import com.avalogics.myshot.prefs.SessionPrefs;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private Retrofit mRestAdapter;
    private UserService mUserService;
    private SignInButton mSignInButtongm;
    // UI references.
    private ImageView mLogoView;
    private Button mSignInButton,mSignInButtonfb;
    private EditText mUserName;
    private EditText mPasswordView;
    private TextInputLayout mFloatLabelUsername;
    private TextInputLayout mFloatLabelPassword;
    private View mProgressView;
    private View mLoginFormView;


    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 420;

    private GoogleApiClient mGoogleApiClient;

    private SignInButton btnSignIn;

    public LoginButton loginButton;
    public Button fb, google;
    public CallbackManager callbackManager;
    public String id, name, email, gender, birthday;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView( R.layout.activity_login);

        /**Fb_Login**/

        callbackManager = CallbackManager.Factory.create();

        fb = (Button) findViewById(R.id.fb);
        google = (Button) findViewById(R.id.google);
        loginButton = (LoginButton) findViewById(R.id.login_button);

        List< String > permissionNeeds = Arrays.asList("user_photos", "email",
                "user_birthday", "public_profile", "AccessToken");
        loginButton.registerCallback(callbackManager, new FacebookCallback< LoginResult >() {@Override
        public void onSuccess(LoginResult loginResult) {

            System.out.println("onSuccess");

            String accessToken = loginResult.getAccessToken()
                    .getToken();
            Log.i("accessToken", accessToken);

            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {@Override
                    public void onCompleted(JSONObject object,
                                            GraphResponse response) {

                        Log.i("LoginActivity",
                                response.toString());
                        try {
                            id = object.getString("id");
                            try {
                                URL profile_pic = new URL(
                                        "http://graph.facebook.com/" + id + "/picture?type=large");
                                Log.i("profile_pic",
                                        profile_pic + "");

                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                            Log.e("UserDate", String.valueOf(object));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields","id,name,email,gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();
        }
            @Override
            public void onCancel() {
                System.out.println("onCancel");
            }
            @Override
            public void onError(FacebookException exception) {
                System.out.println("onError");
                Log.v("LoginActivity", exception.getCause().toString());
            }
        });
        initializeControls();
        initializeGPlusSettings();

        /***Facebook Log**/
        // Crear adaptador Retrofit
        mRestAdapter = new Retrofit.Builder()
                .baseUrl( UserService.BASE_URL)
                .addConverterFactory( GsonConverterFactory.create())
                .build();

        // Crear conexión a la API
        mUserService = mRestAdapter.create(UserService.class);

        //mLogoView = (ImageView ) findViewById(R.id.image_logo);
        mUserName = findViewById( R.id.user_name);
        mPasswordView = findViewById( R.id.password);
        mFloatLabelUsername = findViewById( R.id.float_label_user_name);
        mFloatLabelPassword = findViewById( R.id.float_label_password);
        mSignInButton = findViewById( R.id.email_sign_in_button);
        mLoginFormView = findViewById( R.id.login_form);
        mProgressView = findViewById( R.id.login_progress);
        mSignInButtonfb = findViewById(R.id.fb);
        mSignInButtongm = findViewById( R.id.btn_sign_in);




        // Setup
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    if (!isOnline()) {
                        showLoginError(getString( R.string.error_network));
                        return false;
                    }
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isOnline()) {
                    showLoginError(getString( R.string.error_network));
                    return;
                }
                attemptLogin();

            }
        });




    }


    /**finish Oncreate***/

/*******facebook and gmail validate********/

private void initializeControls(){
    btnSignIn = (SignInButton) findViewById(R.id.btn_sign_in);
    btnSignIn.setOnClickListener(this);
}
    private void initializeGPlusSettings(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        btnSignIn.setSize(SignInButton.SIZE_STANDARD);
        btnSignIn.setScopes(gso.getScopeArray());
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void handleGPlusSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            //Fetch values
            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();
            String familyName = acct.getFamilyName();
            Log.e(TAG, "Name: " + personName +", email: " + email + ", Image: " + personPhotoUrl +", Family Name: " + familyName);
            updateUI(true);
        } else {
            updateUI(false);
        }
    }



    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleGPlusSignInResult(result);
        } else {
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    handleGPlusSignInResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGPlusSignInResult(result);
        }else{
            super.onActivityResult(requestCode, responseCode, data);
            callbackManager.onActivityResult(requestCode, responseCode, data);
        }
    }
    public void onClick(View v) {
        if (v == fb) {
            loginButton.performClick();
        }else if(v == google){
            signIn();
        }
    }

    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            btnSignIn.setVisibility(View.GONE);
        } else {
            btnSignIn.setVisibility(View.VISIBLE);
        }
    }

    /*******End at facebook and gmail validate********/



    private void attemptLogin() {

        // Reset errors.
        mFloatLabelUsername.setError(null);
        mFloatLabelPassword.setError(null);

        // Store values at the time of the login attempt.
        final String username = mUserName.getText().toString();
        final String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

    // Check for a valid password, if the user entered one.
    if (TextUtils.isEmpty( password ) && password != null) {
        mFloatLabelPassword.setError( getString( R.string.error_field_required ) );
        focusView = mFloatLabelPassword;
        cancel = true;
    } else if (!isPasswordValid( password )) {
        mFloatLabelPassword.setError( getString( R.string.error_invalid_password ) );
        focusView = mFloatLabelPassword;
        cancel = true;
    }




        // Verificar si el user tiene contenido.
        if (TextUtils.isEmpty(username) && username != null ) {
            mFloatLabelUsername.setError(getString( R.string.error_field_required));
            focusView = mFloatLabelUsername;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Mostrar el indicador de carga y luego iniciar la petición asíncrona.
            /*showProgress(true);*/

            Call<Affiliate> loginCall = mUserService.login(new LoginBody(username, password));
            loginCall.enqueue(new Callback<Affiliate>() {
                @Override
                public void onResponse(Call<Affiliate> call, Response<Affiliate> response) {
                    // Mostrar progreso
                  /*  showProgress(false);*/



                    // Procesar errores
                    if (!response.isSuccessful()) {
                        String error = "Ha ocurrido un error  Contacte al administrador";
                        if (response.errorBody()
                                .contentType()
                                .subtype()
                                .equals("json")) {
                            ApiError apiError = ApiError.fromResponseBody(response.errorBody());

                            error = apiError.getMessage();
                            //Log.d("LoginActivity", apiError.getDeveloperMessage());
                            Toast.makeText( LoginActivity.this, "Usuario o contraseña invalidos", Toast.LENGTH_SHORT ).show();

                        } else {
                            try {
                                // Reportar causas de error no relacionado con la API
                                Log.d("LoginActivity", response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        showLoginError(error);
                        return;
                    }

                    // Guardar registrado en preferencias
                    SessionPrefs.get( LoginActivity.this).saveAffiliate(response.body());

                    showAppointmentsScreen();
                }

                @Override
                public void onFailure(Call<Affiliate> call, Throwable t) {
                    /*showProgress(false);*/
                    showLoginError(t.getMessage());
                }
            });
        }
    }



    private boolean isPasswordValid(String password) {
        return password.length() > 3;
    }

  private void showProgress(boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);

        int visibility = show ? View.GONE : View.VISIBLE;
        //mLogoView.setVisibility(visibility);
        mLoginFormView.setVisibility(visibility);
    }


    private void showAppointmentsScreen() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void showLoginError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager ) getSystemService( Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }


    public void register(View view) {
        Intent register  = new Intent(LoginActivity.this,Register.class);
        startActivity( register );
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    Intent intent = new Intent( LoginActivity.this,LoginActivity.class );
    startActivity( intent );


    }




}

