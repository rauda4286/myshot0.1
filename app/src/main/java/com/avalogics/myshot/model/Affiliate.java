package com.avalogics.myshot.model;


public class Affiliate {

    private String id;
    private String name;
    private String gmail;
    private String gender;


    public Affiliate(String id, String name, String gmail, String gender) {
        this.id = id;
        this.name = name;
        this.gmail = gmail;
        this.gender = gender;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGmail() {
        return gmail;
    }

    public void setGmail(String address) {
        this.gmail = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}
