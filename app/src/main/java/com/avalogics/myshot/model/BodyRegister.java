package com.avalogics.myshot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BodyRegister {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("login")
        @Expose
        private Login login;
        @SerializedName("facebook_id")
        @Expose
        private Integer facebookId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("picture")
        @Expose
        private String picture;
        @Expose
        private Integer v;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Login getLogin() {
            return login;
        }

        public void setLogin(Login login) {
            this.login = login;
        }

        public Integer getFacebookId() {
            return facebookId;
        }

        public void setFacebookId(Integer facebookId) {
            this.facebookId = facebookId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

