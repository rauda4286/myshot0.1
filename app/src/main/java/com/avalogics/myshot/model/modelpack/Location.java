package com.avalogics.myshot.model.modelpack;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

    public class Location {

        @SerializedName("lat")
        @Expose
        private Double lat;
        @SerializedName("lon")
        @Expose
        private Double lon;

        public Location(String target) {
        }

        public Double getLat() {
            return lat;
        }

        public void setLat(Double lat) {
            this.lat = lat;
        }

        public Double getLon() {
            return lon;
        }

        public void setLon(Double lon) {
            this.lon = lon;
        }


    }
