package com.avalogics.myshot.model;

import com.google.gson.annotations.SerializedName;


public class LoginBody {
    @SerializedName("username")
    private String username;
    private String password;
    private String fbid;
    private String gmid;

    public LoginBody(String username, String password) {
        this.username = username;
        this.password = password;
        this.fbid = fbid;
        this.gmid = gmid;
    }

    public String getUsername() {
        return username;
    }

    public String getFbid() {
        return fbid;
    }

    public void setFbid(String fbid) {
        this.fbid = fbid;
    }

    public String getGmid() {
        return gmid;
    }

    public void setGmid(String gmid) {
        this.gmid = gmid;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
