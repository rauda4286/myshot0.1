package com.avalogics.myshot.model;

public class Comment_Body {
    private String username;
    private String mcomment;



    public Comment_Body(String username,String mComment ){
        this.username = username;
        this.mcomment = mComment;

    }

    public Comment_Body(String comment) {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getMcomment() {
        return mcomment;
    }

    public void setMcomment(String mcomment) {
        this.mcomment = mcomment;
    }

}
