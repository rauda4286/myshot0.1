package com.avalogics.myshot.model;

import com.google.gson.annotations.SerializedName;

public class RegisterBody {

    @SerializedName("username")
    private String username;
    private String password;
    private String email;
    private String gmail_id;
    private String facebook_id;
    private String picture;



    public RegisterBody(String username, String password, String gmail_id) {
        this.username = username;
        this.password = password;
        this.gmail_id = gmail_id;
        this.facebook_id = facebook_id;
        this.email = email;

    }


    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGmail_id() {
        return gmail_id;
    }

    public void setGmail_id(String gmail_id) {
        this.gmail_id = gmail_id;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }
}
