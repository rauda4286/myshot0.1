package com.avalogics.myshot.model.modelpack;


import java.util.List;

import com.avalogics.myshot.model.Pojos.Offer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Offert {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("overdue")
    @Expose
    private String overdue;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("place")
    @Expose
    private Place place;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("usersTaken")
    @Expose
    private List<Object> usersTaken = null;


    public Offert(String id,String code,String image,String place,String title){
        this.id = id;
        this.code = code;
        this.image = image;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOverdue() {
        return overdue;
    }

    public void setOverdue(String overdue) {
        this.overdue = overdue;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public List<Object> getUsersTaken() {
        return usersTaken;
    }

    public void setUsersTaken(List<Object> usersTaken) {
        this.usersTaken = usersTaken;
    }

}