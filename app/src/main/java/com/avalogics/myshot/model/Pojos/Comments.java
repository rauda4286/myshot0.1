package com.avalogics.myshot.model.Pojos;

import java.util.List;

import com.avalogics.myshot.model.modelpack.Comment_;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.w3c.dom.Comment;

public class Comments {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("comments")
    @Expose
    private List<Comment_> comments = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Comment_> getComments() {
        return comments;
    }

    public void setComments(List<Comment_> comments) {
        this.comments = comments;
    }

}