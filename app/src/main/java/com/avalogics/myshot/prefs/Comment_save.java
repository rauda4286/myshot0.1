package com.avalogics.myshot.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.avalogics.myshot.model.modelpack.Comment_;

public class Comment_save {

    public static final String PREFS_NAME = "MySHot";
    public static final String PREF_AFFILIATE_NAME = "PREF_AFFILIATE_NAME";
    public static final String PREF_AFFILIATE_COMMENT= "PREF_AFFILIATE_GENDER";
    private final SharedPreferences mPrefs;

    private boolean mIsPost = false;

    private static Comment_save INSTANCE;

    public static Comment_save get(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new Comment_save(context);
        }
        return INSTANCE;
    }

    private Comment_save(Context context) {
        mPrefs = context.getApplicationContext()
                .getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public boolean mIsPost() {
        return mIsPost;
    }

    public void savecomments(Comment_ comment) {
        if (comment != null) {
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putString(PREF_AFFILIATE_COMMENT, comment.getComment());
            editor.apply();

            mIsPost = true;
        }
    }
}
