package com.avalogics.myshot.Adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avalogics.myshot.model.Pojos.Offer;
import com.avalogics.myshot.R;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NewAdapter {

    private Context context;
    private ImageView code;
    private List<Offer> ListOf;

    public NewAdapter(Context applicationContext, List<Offer> postList) {
        this.context = applicationContext;
        this.ListOf = postList;
    }

    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate( R.layout.activity_details_offer, parent, false);
        return new NewsViewHolder(view);
    }

    public class NewsViewHolder  {

        public TextView title;
        public TextView description;
        public ImageView code;

        public NewsViewHolder(View itemView) {
            super();
            title = itemView.findViewById( R.id.descript);
            ///description = (TextView ) itemView.findViewById(R.id.post_description);
            code = itemView.findViewById(R.id.qrcode);
        }
    }


    public void onBindViewHolder(NewsViewHolder holder, int position) {
        Offer apiObject = ListOf.get(position);
        holder.title.setText(apiObject.getTitle());
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(ListOf.get(position).getImage())
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(holder.code);


    }


}
