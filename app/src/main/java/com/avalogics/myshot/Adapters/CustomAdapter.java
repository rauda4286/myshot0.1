package com.avalogics.myshot.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avalogics.myshot.MainActivity;
import com.avalogics.myshot.layouts.Commerc_details;
import com.avalogics.myshot.model.Pojos.Offer;
import com.avalogics.myshot.model.Pojos.OffersD;
import com.avalogics.myshot.R;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.CustomViewHolder> implements  View.OnClickListener  {

    private List <OffersD> offerList;
    private Context context;
    private View.OnClickListener listener;




    public CustomAdapter(Context context, List <OffersD> offersList ){
        this.context = context;
        this.offerList = offersList;
    }


    class CustomViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        TextView txtTitle,txtType,txtDistance;
        private ImageView coverImage;

        CustomViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            txtTitle = mView.findViewById( R.id.title);
            coverImage = mView.findViewById( R.id.coverImage);
            txtType = mView.findViewById( R.id.type);
            txtDistance = mView.findViewById(R.id.distance);


            /****OnClickevent at Recycler on MainActivity****/

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION)
                    {
                        OffersD clickedDataItem = offerList.get(pos);
                        Intent intent = new Intent(context, Commerc_details.class);
                        intent.putExtra("image", offerList.get(pos).getImage());
                        intent.putExtra("name", offerList.get(pos).getName());
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        Toast.makeText(v.getContext(), "You clicked " + clickedDataItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                }

            });


        }
    }

    @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.custom_row, parent, false);
        view.setOnClickListener( this );
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {

        holder.txtTitle.setText(offerList.get(position).getName());
        //holder.txtDistance.setText( offerList.get(position).getDistance());
        //holder.txtType.setText(offerList.get(position).getTypes());




        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(offerList.get(position).getImage())
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(holder.coverImage);


    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }


    public void setOnclickListener(View.OnClickListener listener){
        this.listener = listener;

    }

    public void onClick(View v) {

        if(listener != null){
            listener.onClick( v );
        }

    }
}