package com.avalogics.myshot.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avalogics.myshot.layouts.Details_offer;
import com.avalogics.myshot.model.Pojos.Offer;
import com.avalogics.myshot.R;
import com.avalogics.myshot.layouts.Commerc_details;
import com.avalogics.myshot.model.Pojos.OffersD;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.List;


public class DetailsAdapter extends  RecyclerView.Adapter<DetailsAdapter.DetailsViewHolder> implements  View.OnClickListener {


    private List <Offer> offerDet;
    private Context context;
    private View.OnClickListener listenerdet;



   public DetailsAdapter(Commerc_details context, List <Offer> offerDet) {
        this.context = context;
        this.offerDet = offerDet;
    }


    public class DetailsViewHolder extends RecyclerView.ViewHolder {

        public View mView;
        TextView txtTitle;
        private ImageView imageoffer, gallery,Qr;


        public DetailsViewHolder(View itemView) {
            super( itemView );
            mView = itemView;

            txtTitle = mView.findViewById( R.id.titleoffer );
            imageoffer = mView.findViewById( R.id.imageoffer );
            gallery = mView.findViewById( R.id.gallery );

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION)
                    {
                        Offer clickedDataItem = offerDet.get(pos);
                        Intent intent = new Intent(context, Details_offer.class);
                        intent.putExtra("Title", offerDet.get(pos).getImage());
                        intent.putExtra("Image", offerDet.get(pos).getTitle());
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        Toast.makeText(v.getContext(), "You clicked " + clickedDataItem.getImage(), Toast.LENGTH_SHORT).show();
                    }
                }

            });

        }
    }

    @NonNull
    @Override
    public DetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.offer_row, null, false );
        view.setOnClickListener( this );
        return new DetailsViewHolder( view );
    }

    @Override
    public void onBindViewHolder(DetailsViewHolder holder, int position) {
        holder.txtTitle.setText( offerDet.get( position ).getTitle() );


        Picasso.Builder builder = new Picasso.Builder( context );
        builder.downloader( new OkHttp3Downloader( context ) );
        builder.build().load( offerDet.get( position ).getImage() )
                .placeholder( (R.drawable.ic_launcher_background) )
                .error( R.drawable.ic_launcher_background )
                .into( holder.imageoffer );

       /* builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(offerLD.get(position).getGallery())
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(holder.gallery);*/


    }

    @Override
    public int getItemCount() {
        return offerDet.size();
    }

    public void setOnclickListener(View.OnClickListener listenerdet) {
        this.listenerdet = listenerdet;


    }

    public void onClick(View v) {

        if (listenerdet != null) {
            listenerdet .onClick( v );
        }
    }
}