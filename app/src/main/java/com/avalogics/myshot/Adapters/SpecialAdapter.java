package com.avalogics.myshot.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avalogics.myshot.R;
import com.avalogics.myshot.layouts.Special_Offers;
import com.avalogics.myshot.model.modelpack.Offert;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SpecialAdapter extends RecyclerView.Adapter<SpecialAdapter.SpecialViewHolder> implements View.OnClickListener {


    List <Offert> speciallist;
    Special_Offers context;
    View.OnClickListener listener;



    public SpecialAdapter(Special_Offers context, List <Offert> special_list){
        this.context = context;
        this.speciallist = special_list;
    }


    @NonNull
    @Override
    public SpecialAdapter.SpecialViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from( parent.getContext() );
        View view = layoutInflater.inflate( R.layout.custom_row, parent, false );
        view.setOnClickListener( this );
        return new SpecialAdapter.SpecialViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SpecialAdapter.SpecialViewHolder holder, int position) {

        holder.txtSpecial.setText(speciallist.get(position).getTitle());


        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(speciallist.get(position).getImage())
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(holder.special_image);





    }

    @Override
    public int getItemCount() {
            return speciallist.size();
    }

    @Override
    public void onClick(View v) {

    }

    public void setOnclickListener(View.OnClickListener listener) {
        this.listener = listener;
    }


    public class SpecialViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        private TextView txtSpecial;
        private ImageView special_image;



        public SpecialViewHolder(View itemView) {
            super( itemView );
            View mView;

            txtSpecial = itemView.findViewById( R.id.Spetecialtitle );
            special_image = itemView.findViewById( R.id.coverspecial );





        }
    }
}
