package com.avalogics.myshot.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avalogics.myshot.R;
import com.avalogics.myshot.model.modelpack.Comment_;

import java.util.ArrayList;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> implements View.OnClickListener {

    private ArrayList<Comment_> android;

    public CommentAdapter(ArrayList<Comment_> android) {
        this.android = android;
    }

    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder( ViewHolder holder, int position) {
        holder.tv_name.setText(android.get(position).getComment());

    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    @Override
    public void onClick(View view) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;


        public ViewHolder(View itemView) {
            super(itemView);

            tv_name = (TextView)itemView.findViewById(R.id.comment_id);


        }
    }

    public class CommetViewHolder {
    }
}