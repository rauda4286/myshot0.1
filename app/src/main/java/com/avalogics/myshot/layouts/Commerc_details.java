package com.avalogics.myshot.layouts;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.avalogics.myshot.Adapters.DetailsAdapter;
import com.avalogics.myshot.MainActivity;
import com.avalogics.myshot.Networking.GetDataService;
import com.avalogics.myshot.Networking.RetrofitClientInstance;
import com.avalogics.myshot.model.Pojos.Offer;
import com.avalogics.myshot.R;
import com.avalogics.myshot.model.Pojos.OffersD;
import com.bumptech.glide.Glide;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Commerc_details extends AppCompatActivity {

    private DetailsAdapter detadapter;
    private RecyclerView mrecyclerView;
    ProgressDialog progressDoalog;
    private List <Offer> offerLis;
    private Button btnInsta,btnwazw,btnmaps,btnface,btnexit,btncom;
    private ImageView gallerry;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.commerc_details );

        btnInsta = findViewById( R.id. inst);
        btnface = findViewById( R.id. face);
        btnmaps = findViewById( R.id. google);
        btnwazw = findViewById( R.id. waze);
        btnexit = findViewById( R.id.exit );
        btncom = findViewById( R.id.ShowComent );
        gallerry = findViewById( R.id.gallery);


        /***************OnClick Event that Buttons***************/
        btnexit.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent exit = new Intent( Commerc_details.this,MainActivity.class );
                startActivity( exit );
            }
        } );

        btnmaps.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( Commerc_details.this, "Maps", Toast.LENGTH_SHORT ).show();
            }
        } );
        btnwazw.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( Commerc_details.this, "waze", Toast.LENGTH_SHORT ).show();
            }
        } );
        btnface.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( Commerc_details.this, "facebook", Toast.LENGTH_SHORT ).show();
            }
        } );
        btnInsta.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( Commerc_details.this, "Instagram", Toast.LENGTH_SHORT ).show();
            }
        } );


        progressDoalog = new ProgressDialog( Commerc_details.this );
        progressDoalog.setMessage( "Loading...." );
        progressDoalog.show();


        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create( GetDataService.class );
        Call<List<Offer>> call = service.getDetails();
        call.enqueue( new Callback<List <Offer>>() {
            @Override
            public void onResponse(Call <List <Offer>> call, Response<List <Offer>> response) {
                progressDoalog.dismiss();
                generateOfferList( response.body() );
            }

            @Override
            public void onFailure(Call <List <Offer>> call, Throwable t) {

                progressDoalog.dismiss();

                Toast.makeText( Commerc_details.this, "Something went wrong...Please try again later!", Toast.LENGTH_SHORT ).show();

            }

        } );
    }


    private void generateOfferList(List<Offer> offerDet) {
        mrecyclerView = findViewById(R.id.mrecyclerView);
        detadapter = new DetailsAdapter(this,offerDet);
        mrecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mrecyclerView.setAdapter(detadapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent back1 = new Intent(Commerc_details.this,MainActivity.class );
        startActivity( back1 );
    }


    public void posted(View view) {
        Intent intent = new Intent(Commerc_details.this,Comment_Activity.class );
        startActivity( intent );

    }
}


