package com.avalogics.myshot.layouts;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.avalogics.myshot.Adapters.CommentAdapter;
import com.avalogics.myshot.Networking.GetDataService;
import com.avalogics.myshot.Networking.UserService;
import com.avalogics.myshot.R;
import com.avalogics.myshot.model.Pojos.Comments;
import com.avalogics.myshot.model.modelpack.Comment_;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Comment_Activity extends AppCompatActivity{

    private Retrofit RestAdapter;
    private UserService mUserService;
    private EditText Comment;
    private TextView post;
    private Comments comments;

    /**************/
    private CommentAdapter adapter;
    private RecyclerView recyclerView;
    private ArrayList<Comment_> data;
    /*************/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_comment_);

        post = findViewById( R.id.btnpublish );
        Comment = findViewById( R.id.postComm);

        // Crear adaptador Retrofit
        RestAdapter = new Retrofit.Builder()
                .baseUrl( UserService.BASE_URL)
                .addConverterFactory( GsonConverterFactory.create())
                .build();

        // Crear conexión a la API
        mUserService = RestAdapter.create(UserService.class);

/*************Get comments************/
        initViews();

/***********************************/

        /*post.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isOnline()) {
                    Toast.makeText( Comment_Activity.this, "Porfavor verifique conexion a internet", Toast.LENGTH_SHORT ).show();
                    return;
                }
                attemptPost();
            }
        } );*/

    }

    private void initViews() {
        recyclerView = (RecyclerView)findViewById(R.id.mrecyclercomments);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        loadJSON();
    }


    private void loadJSON() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ec2-18-191-167-26.us-east-2.compute.amazonaws.com:5012")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        GetDataService request = retrofit.create(GetDataService.class);
        Call<Comments> call = request.getComment();
        call.enqueue(new Callback<Comments>() {
            @Override
            public void onResponse(Call<Comments> call, Response<Comments> response) {

                //Comment_ jsonResponse = response.body();
                //data = new ArrayList<>(Arrays.asList(jsonResponse.getComments()));
                //Comment_ comm = (Comment_) response.body().getComments();
                ArrayList<Comment_> Comm = response.body().getComments();
                adapter = new CommentAdapter(data);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Comments> call, Throwable t) {
                Log.d("Error",t.getMessage());
            }
        });
    }

   /* private void attemptPost() {

        // Store values at the time of the login attempt.

        String comment = Comment.getText().toString();


            Call<Comment_> registerCall = mUserService.comment(new Comment_Body(comment));
            registerCall.enqueue(new Callback<Comment_>() {
                @Override
                public void onResponse(Call<Comment_> call, Response<Comment_> response) {

                    // Guardar registrado en preferencia
                    Comment_save.get( Comment_Activity.this).savecomments(response.body());

                }

                @Override
                public void onFailure(Call<Comment_> call, Throwable t) {

                    Toast.makeText( Comment_Activity.this, "Algo ocurre :O", Toast.LENGTH_SHORT ).show();

                }
            });*/
        }



