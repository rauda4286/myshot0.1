package com.avalogics.myshot.layouts;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.avalogics.myshot.Adapters.SpecialAdapter;
import com.avalogics.myshot.Networking.GetDataService;
import com.avalogics.myshot.Networking.RetrofitClientInstance;
import com.avalogics.myshot.R;
import com.avalogics.myshot.model.modelpack.Offert;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Special_Offers extends AppCompatActivity {

    private SpecialAdapter sadapter;
    private RecyclerView sRecyler;
    private List<Offert> speciallist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.special__offers );


        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create( GetDataService.class );
        Call<List<Offert>> call = service.getOffers();
        call.enqueue( new Callback<List <Offert>>() {
            @Override
            public void onResponse(Call <List <Offert>> call, Response<List <Offert>> response) {


                genertaleSpecialOfferts( response.body() );
            }

            @Override
            public void onFailure(Call <List <Offert>> call, Throwable t) {

                Toast.makeText( Special_Offers.this, "Something went wrong...Please try again later!", Toast.LENGTH_SHORT ).show();

            }

        } );
    }

    private void genertaleSpecialOfferts(List<Offert> body) {

        sRecyler = findViewById(R.id.specailRecycler);
       sadapter = new SpecialAdapter(this,body);
        sRecyler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        sRecyler.setAdapter(sadapter);


        sadapter.setOnclickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent( Special_Offers.this, Details_offer.class );
                startActivity( i );

            }
        } );


    }
}
