package com.avalogics.myshot.layouts;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.avalogics.myshot.Adapters.NewAdapter;
import com.avalogics.myshot.Networking.GetDataService;
import com.avalogics.myshot.Networking.RetrofitClientInstance;
import com.avalogics.myshot.model.Pojos.Offer;
import com.avalogics.myshot.R;
import com.bumptech.glide.Glide;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Details_offer extends AppCompatActivity {

    private TextView comerce_name;
    private ImageView imageView;
    private Button btnExit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_details_offer );


        comerce_name = findViewById( R.id.descript );
        imageView = findViewById( R.id.qrcode );
        btnExit = findViewById( R.id.exit2 );


        String commerce_name = getIntent().getExtras().getString("title");
        String image_view = getIntent().getExtras().getString("code");


        comerce_name.setText(commerce_name);

        Glide.with(this)
                .load(image_view)
                .placeholder(R.drawable.logo)
                .into(imageView);


        /******Onclick_Event to Exit  "X" *****/
        btnExit.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ex = new Intent(Details_offer.this,Commerc_details.class );
                startActivity( ex );
            }
        } );


        }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent back = new Intent(Details_offer.this,Commerc_details.class );
        startActivity( back );
    }
}

