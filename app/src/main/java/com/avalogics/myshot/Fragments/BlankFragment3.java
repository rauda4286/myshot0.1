package com.avalogics.myshot.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avalogics.myshot.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class BlankFragment3 extends Fragment implements OnMapReadyCallback {

    GoogleMap mGoogleMap;
    MapView mapView;
    View maView;
    private LatLng latLng;



    public BlankFragment3() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        maView = inflater.inflate( R.layout.fragment_blank_fragment3, container, false);

        return  maView;
    }

    @Override
    public void onViewCreated( View view, Bundle savedInstanceState) {
        super.onViewCreated( view, savedInstanceState );

        mapView = (MapView) maView.findViewById( R.id.maps3 );
        if(mapView != null ){
            mapView.onCreate( null );
            mapView.onResume();
            mapView.getMapAsync( this );
        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize( getContext() );
        mGoogleMap = googleMap;
        googleMap.setMapType( GoogleMap.MAP_TYPE_NORMAL);


        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(13.721396, -88.9343195))
                .title("Hello Rauda"));

    }
}
