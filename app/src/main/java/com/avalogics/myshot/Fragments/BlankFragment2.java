package com.avalogics.myshot.Fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.avalogics.myshot.Adapters.CustomAdapter;
import com.avalogics.myshot.MainActivity;
import com.avalogics.myshot.Networking.GetDataService;
import com.avalogics.myshot.Networking.RetrofitClientInstance;
import com.avalogics.myshot.R;
import com.avalogics.myshot.model.Pojos.OffersD;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BlankFragment2 extends Fragment implements OnMapReadyCallback {

    GoogleMap mGoogleMap;
    MapView mapView;
    View maView;
    private LatLng latLng;
    RecyclerView recyclerView;
    List <OffersD> listCont;
    CustomAdapter adapter;


    public BlankFragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        maView = inflater.inflate( R.layout.fragment_blank_fragment3, container, false );
        /*recyclerView = (RecyclerView ) maView.findViewById(R.id.contact_recycleView);
        CustomAdapter viewAdapter = new CustomAdapter(getContext(), listCont);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(viewAdapter);*/
        return maView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated( view, savedInstanceState );

        mapView = ( MapView ) maView.findViewById( R.id.maps3 );
        if (mapView != null) {
            mapView.onCreate( null );
            mapView.onResume();
            mapView.getMapAsync( this );
        }

    }

    /*@Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );


        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class );
        Call<List <OffersD>> call = service.getData();
        call.enqueue( new Callback<List <OffersD>>() {
            @Override
            public void onResponse(Call <List <OffersD>> call, Response<List <OffersD>> response) {
                generateDataList( response.body() );
            }

            @Override
            public void onFailure(Call <List <OffersD>> call, Throwable t) {
                Toast.makeText( getContext(), "Parece que tenemos problemas, porfavor intente mas tarde", Toast.LENGTH_SHORT ).show();
            }
        } );



    }*/


    /*private void generateDataList(List <OffersD> body) {
        recyclerView = maView.findViewById(R.id.contact_recycleView);
        adapter = new CustomAdapter(getContext(),body);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);
    }*/


    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize( getContext() );
        mGoogleMap = googleMap;
        googleMap.setMapType( GoogleMap.MAP_TYPE_NORMAL );


        googleMap.addMarker( new MarkerOptions()
                .position( new LatLng( 13.421396, -88.9343195 ) )
                .title( "Hello Rauda" ) );

        if (ActivityCompat.checkSelfPermission( getContext(), Manifest.permission.ACCESS_FINE_LOCATION )
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( getContext()
                , Manifest.permission.ACCESS_COARSE_LOCATION )
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setMyLocationEnabled( true );




    }
}
