package com.avalogics.myshot.Fragments;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avalogics.myshot.R;
import com.avalogics.myshot.layouts.MyLocation;
import com.avalogics.myshot.model.modelpack.Location;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;


public class BlankFragment extends Fragment implements OnMapReadyCallback {


    GoogleMap mGoogleMap;
    MapView mapView;
    View mView;


    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate( R.layout.fragment_blank_fragment3, container, false );

        return mView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated( view, savedInstanceState );

        mapView = ( MapView ) mView.findViewById( R.id.maps3 );
        if (mapView != null) {
            mapView.onCreate( null );
            mapView.onResume();
            mapView.getMapAsync( this );
        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize( getContext() );
        mGoogleMap = googleMap;
        googleMap.setMapType( GoogleMap.MAP_TYPE_NORMAL );

        LatLng burgerbanc = new LatLng(13.421396, -13.421396);
        googleMap.addMarker( new MarkerOptions()
                .position( new LatLng( 13.421396, -88.9343195 ) )
                .title( "Hello Rauda" ) );

        if (ActivityCompat.checkSelfPermission( getContext(), Manifest.permission.ACCESS_FINE_LOCATION )
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( getContext()
                , Manifest.permission.ACCESS_COARSE_LOCATION )
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setMyLocationEnabled( true );
        float zoomLevel = 16.0f; //This goes up to 21

    }

    }


